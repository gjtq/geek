import { Link, useHistory } from 'react-router-dom'
import styles from './index.module.scss'
import { useEffect, useRef, useState } from 'react'
function NotFound() {
  const [count, setCount] = useState(10)
  const timerId = useRef(0)
  const history = useHistory()
  useEffect(() => {
    timerId.current = setInterval(() => {
      setCount((count) => count - 1)
    }, 1000)
    return () => clearInterval(timerId.current)
  }, [])
  useEffect(() => {
    if (count === 0) {
      history.replace('/home')
    }
  }, [count])
  return (
    <div className={styles.root}>
      <h1>对不起，您访问的页面不存在~</h1>
      <p className="back">
        将在 {count} 秒后，返回首页（或者：点击立即返回
        <Link to="/home">首页</Link>）
      </p>
    </div>
  )
}

export default NotFound
