import Child from './components/child'
// import './index.scss'
// 样式模块化写法（推荐）
import style from './index.module.scss'
console.log(style)
function Test() {
  return (
    <div>
      <h1 className="red">Test</h1>
      {/* <h1 className={[style.red, style.borders].join(' ')}>Test</h1> */}
      <h1 className={`${style.red} ${style.borders}`}>Test</h1>
      <p className={style.borders}>border</p>
      <p className={style['font-blue']}>蓝色字体</p>
      <hr />
      <Child />
    </div>
  )
}

export default Test
