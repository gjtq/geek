import {
  Card,
  Breadcrumb,
  Form,
  Button,
  Radio,
  Input,
  Upload,
  Space,
  message,
  Spin,
} from 'antd'
import { PlusOutlined } from '@ant-design/icons'
import { Link, useHistory, useParams } from 'react-router-dom'
import styles from './index.module.scss'

// 富文本编辑器组件和样式
import ReactQuill from 'react-quill'
import 'react-quill/dist/quill.snow.css'
import Channel from '@/components/channel'
import { useEffect, useRef, useState } from 'react'
import { useDispatch } from 'react-redux'
import { addArticleAction } from '@/store/actions/article'
import request from '@/utils/request'

const Publish = () => {
  const dispatch = useDispatch()
  const history = useHistory()
  const params = useParams()
  // 1. 上传文章封面
  const [fileList, setFileList] = useState([])
  const onUploadChange = (data) => {
    const _fileList = data.fileList.map((file) => {
      console.log(file)
      if (file.response) {
        return {
          url: file.response.data.url,
        }
      }
      return file
    })
    fileListRef.current = _fileList
    setFileList(_fileList)
  }

  // 2. 控制图片封面上传的数量
  const [maxCount, setMaxCount] = useState(1)
  const fileListRef = useRef([])
  const changeType = (e) => {
    const count = e.target.value
    setMaxCount(count)
    if (count === 1) {
      const firstImg = fileListRef.current[0]
      setFileList(!firstImg ? [] : [firstImg])
    } else if (count === 3) {
      setFileList(fileListRef.current)
    }
  }

  // 3. 发布文章
  const onFinish = async (formdata) => {
    publishArticle(formdata, false)
  }

  // 4. 存入草稿
  const [form] = Form.useForm()
  const saveDraft = async () => {
    try {
      const formData = await form.validateFields()
      publishArticle(formData, true)
    } catch (error) {
      console.log(error)
    }
  }

  // 5. 编辑状态填充
  useEffect(() => {
    if (!!!params.id) return setLoading(false)
    // 编辑状态
    const getDetail = async () => {
      const { data } = await request.get(`/mp/articles/${params.id}`)
      const {
        title,
        content,
        channel_id,
        cover: { type, images },
      } = data
      const formData = { type, title, content, channel_id }
      form.setFieldsValue(formData)
      const imgList = images.map((item) => ({ url: item }))
      setFileList(imgList)
      fileListRef.current = imgList
      setMaxCount(type)
      setLoading(false)
    }

    getDetail()
  }, [])

  // 添加loading效果
  const [loading, setLoading] = useState(true)

  // 抽取方法
  const publishArticle = async (formdata, isDraft, isEdit) => {
    const { type, ...sxd } = formdata
    // 校验图片
    if (type !== fileList.length) {
      return message.error('封面数量和上传图片数量不一致!')
    }
    const data = {
      ...sxd,
      cover: {
        type,
        images: fileList.map((item) => item.url),
      },
    }
    if (!!params.id) {
      data.id = params.id
    }
    try {
      await dispatch(addArticleAction(data, isDraft, !!params.id))
      message.success(isDraft ? '存为草稿成功' : '发布成功')
      history.push('/home/article')
    } catch (error) {
      console.log(error)
    }
  }
  return (
    <div className={styles.root}>
      <Spin tip="努力加载中..." spinning={loading}>
        <Card
          title={
            <Breadcrumb separator=">">
              <Breadcrumb.Item>
                <Link to="/home">首页</Link>
              </Breadcrumb.Item>
              <Breadcrumb.Item>
                {!!params.id ? '编辑文章' : '发布文章'}
              </Breadcrumb.Item>
            </Breadcrumb>
          }
        >
          <Form
            form={form}
            onFinish={onFinish}
            labelCol={{ span: 4 }}
            wrapperCol={{ span: 16 }}
            initialValues={{ type: 1 }}
          >
            <Form.Item
              label="标题"
              name="title"
              rules={[{ required: true, message: '请输入文章标题' }]}
            >
              <Input placeholder="请输入文章标题" style={{ width: 400 }} />
            </Form.Item>
            <Form.Item
              label="频道"
              name="channel_id"
              rules={[{ required: true, message: '请选择文章频道' }]}
            >
              <Channel />
            </Form.Item>

            <Form.Item label="封面">
              <Form.Item name="type">
                <Radio.Group onChange={changeType}>
                  <Radio value={1}>单图</Radio>
                  <Radio value={3}>三图</Radio>
                  <Radio value={0}>无图</Radio>
                  {/* <Radio value={-1}>自动</Radio> */}
                </Radio.Group>
              </Form.Item>
              {/* Upload 组件说明： */}
              {maxCount > 0 && (
                <Upload
                  maxCount={maxCount}
                  className="avatar-uploader"
                  // 发到后台的文件参数名
                  // 必须指定，根据接口文档的说明，需要设置为 image
                  name="image"
                  // 上传组件展示方式
                  listType="picture-card"
                  // 展示已上传图片列表
                  showUploadList
                  // 接口地址
                  // 注意：Upload 再上传图片时，默认不会执行 axios 的请求，所以，此处需要手动设置完整接口地址
                  action="http://geek.itheima.net/v1_0/upload"
                  // 多选
                  multiple={maxCount > 1}
                  // 已经上传的文件列表，设置该属性后组件变为 受控
                  fileList={fileList}
                  // 上传文件改变时的回调
                  onChange={onUploadChange}
                >
                  <div style={{ marginTop: 8 }}>
                    <PlusOutlined />
                  </div>
                </Upload>
              )}
            </Form.Item>
            <Form.Item
              label="内容"
              name="content"
              rules={[{ required: true, message: '请输入文章内容' }]}
            >
              {/* 富文本编辑器 */}
              {/* <ReactQuill style={{ height: 300 }} /> */}
              <ReactQuill />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 4 }}>
              <Space>
                <Button size="large" type="primary" htmlType="submit">
                  {!!params.id ? '编辑文章' : '发布文章'}
                </Button>
                <Button size="large" onClick={saveDraft}>
                  存入草稿
                </Button>
              </Space>
            </Form.Item>
          </Form>
        </Card>
      </Spin>
    </div>
  )
}

export default Publish
