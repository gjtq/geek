import { Link, useHistory } from 'react-router-dom'
import {
  Card,
  Breadcrumb,
  Form,
  Button,
  Radio,
  DatePicker,
  Space,
  Table,
  Tag,
  Modal,
} from 'antd'

import {
  EditOutlined,
  DeleteOutlined,
  WarningOutlined,
} from '@ant-design/icons'

import { useDispatch, useSelector } from 'react-redux'
import { useEffect, useRef } from 'react'
import {
  delArticleAction,
  getArticleAction,
  getChannelAction,
} from '@/store/actions/article'

import img404 from '@/assets/error.png'
import { articleStatus } from '@/const/article'
import Channel from '@/components/channel'
// import 'moment/locale/zh-cn'
// import locale from 'antd/es/date-picker/locale/zh_CN'

const { RangePicker } = DatePicker

const Article = () => {
  const history = useHistory()
  const columns = [
    {
      title: '封面',
      dataIndex: 'cover',
      render: (cover) => {
        return <img src={cover || img404} width={200} height={150} alt="" />
      },
    },
    {
      title: '标题',
      dataIndex: 'title',
      width: 220,
    },
    {
      title: '状态',
      dataIndex: 'status',
      render: (status) => (
        <Tag color={articleStatus[status].color}>
          {articleStatus[status].text}
        </Tag>
      ),
    },
    {
      title: '发布时间',
      dataIndex: 'pubdate',
    },
    {
      title: '阅读数',
      dataIndex: 'read_count',
    },
    {
      title: '评论数',
      dataIndex: 'comment_count',
    },
    {
      title: '点赞数',
      dataIndex: 'like_count',
    },
    {
      title: '操作',
      render: (data) => {
        return (
          <Space size="middle">
            {/* 修改文章 */}
            <Button
              onClick={() => history.push(`/home/publish/${data.id}`)}
              type="primary"
              shape="circle"
              icon={<EditOutlined />}
            />
            {/* 删除文章 */}
            <Button
              type="primary"
              danger
              shape="circle"
              onClick={() => delArticle(data)}
              icon={<DeleteOutlined />}
            />
          </Space>
        )
      },
    },
  ]

  const dispatch = useDispatch()
  // const history = useHistory()
  // 1.获取文章频道列表数据 - 抽取到公共组件了
  // const { channels } = useSelector((state) => state.article)
  // useEffect(() => {
  //   dispatch(getChannelAction())
  // }, [])

  // 2.获取文章列表数据
  const { list, total } = useSelector((state) => state.article)
  useEffect(() => {
    dispatch(getArticleAction({}))
  }, [])

  // 3. 根据表单选择的条件过滤文章列表
  const onFilter = ({ status, channel_id, date }) => {
    const params = { channel_id }
    // 排除全部，因为不传是全部
    if (status !== -1) {
      params.status = status
    }
    // 判断是否选择日期
    if (!!date) {
      // 开始时间
      params.begin_pubdate = date[0].format('YYYY-MM-DD HH:mm:ss')
      // 结束时间
      params.end_pubdate = date[1].format('YYYY-MM-DD HH:mm:ss')
    }
    filters.current = params
    dispatch(getArticleAction(params))
  }

  // 4. 分页
  const filters = useRef({})
  const { page, pageSize } = useSelector((state) => state.article)
  const onPageChange = (page, pageSize) => {
    const params = {
      page,
      per_page: pageSize,
      // 加上过滤条件数据
      ...filters.current,
    }
    // 根据最新分页数据刷新列表
    dispatch(getArticleAction(params))
  }

  // 5. 删除文章
  const delArticle = (article) => {
    Modal.confirm({
      title: '提示',
      content: `确认删除:${article.title}吗?`,
      icon: <WarningOutlined />,
      onOk() {
        dispatch(delArticleAction(article.id, filters.current))
      },
    })
  }

  return (
    <>
      <Card
        title={
          // 卡片的头
          // 面包屑
          <Breadcrumb separator=">">
            <Breadcrumb.Item>
              <Link to="/home">首页</Link>
            </Breadcrumb.Item>
            <Breadcrumb.Item>内容管理</Breadcrumb.Item>
          </Breadcrumb>
        }
        style={{ marginBottom: 20 }}
      >
        {/* 卡片内容 */}
        <Form onFinish={onFilter} initialValues={{ status: -1 }}>
          <Form.Item label="状态" name="status">
            {/* 单选：文章的状态 */}
            <Radio.Group>
              <Radio value={-1}>全部</Radio>
              <Radio value={0}>草稿</Radio>
              <Radio value={1}>待审核</Radio>
              <Radio value={2}>审核通过</Radio>
              <Radio value={3}>审核失败</Radio>
            </Radio.Group>
          </Form.Item>

          <Form.Item label="频道" name="channel_id">
            <Channel width={400} />
          </Form.Item>

          <Form.Item label="日期" name="date">
            {/* 文章发布日期选择，这里的locale={locale}是指定国际化的，后面详说 */}
            {/* <RangePicker locale={locale}></RangePicker> */}
            <RangePicker></RangePicker>
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit">
              筛选
            </Button>
          </Form.Item>
        </Form>
        {/* table */}
        <Card title={`根据筛选条件获取到${total}条数据`}>
          <Table
            rowKey="id"
            columns={columns}
            dataSource={list}
            pagination={{
              position: ['bottomLeft'],
              current: page,
              pageSize,
              total,
              showSizeChanger: true,
              onChange: onPageChange,
            }}
          />
        </Card>
      </Card>
    </>
  )
}

export default Article
