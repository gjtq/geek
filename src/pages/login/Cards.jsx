function Cards(props) {
  return (
    <div>
      <h1>{props.title}</h1>
      <div className="box">{props.children}</div>
    </div>
  )
}

export default Cards
