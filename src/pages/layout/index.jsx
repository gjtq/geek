import {
  HomeOutlined,
  DiffOutlined,
  EditOutlined,
  LogoutOutlined,
} from '@ant-design/icons'
import React, { lazy, Suspense, useEffect } from 'react'
import { Layout, Menu, Popconfirm } from 'antd'
// 导入组件自定义样式
// import './index.scss'
import style from './index.module.scss'
// 配置子路由
// import Home from '../sublayout/home'
// import Article from '../sublayout/article'
// import Publish from '../sublayout/publish'
import { Route, Link, useLocation, useHistory, Switch } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { getUserAction } from '@/store/actions/user'
import { logoutAction } from '@/store/actions/login'
// import NotFound from '../404'

const Home = lazy(() => import('../sublayout/home'))
const Article = lazy(() => import('../sublayout/article'))
const Publish = lazy(() => import('../sublayout/publish'))
const NotFound = lazy(() => import('@/pages/404'))

const { Header, Sider } = Layout

function Layouts() {
  // const {
  //   token: { colorBgContainer },
  // } = theme.useToken()
  const location = useLocation()
  const dispatch = useDispatch()
  const history = useHistory()
  const user = useSelector((state) => state.user)
  // 1. 实现菜单高亮
  // 当前选中的菜单
  const crrrSelected = location.pathname.startsWith('/home/publish')
    ? '/home/publish'
    : location.pathname

  // 2. 获取登录人信息存储到redux
  useEffect(() => {
    dispatch(getUserAction())
  }, [])

  // 3. 退出功能
  const onLogout = () => {
    dispatch(logoutAction())
    history.replace('/login')
  }
  // 4. 每次路由切换页面，执行滚动回顶
  document.querySelector('.scrollBox') &&
    (document.querySelector('.scrollBox').scrollTop = 0)
  return (
    <Layout className={style.root}>
      {/* 顶部通栏 */}
      <Header className="header">
        {/* 左侧：系统logo */}
        <div className="logo" />
        {/* 右侧：用户信息 */}
        <div className="user-info">
          <span className="user-name">{'银阳科技' || user.name}</span>
          <span className="user-logout">
            <Popconfirm
              title="是否确认退出？"
              onConfirm={onLogout}
              okText="退出"
              cancelText="取消"
            >
              <LogoutOutlined /> 退出
            </Popconfirm>
          </span>
        </div>
      </Header>
      <Layout>
        {/* 左侧：菜单 */}
        <Sider
          width={200}
          // style={{
          //   background: colorBgContainer,
          // }}
        >
          <Menu
            theme="dark"
            mode="inline"
            // defaultSelectedKeys={[crrrSelected]}
            selectedKeys={[crrrSelected]}
            style={{
              height: '100%',
              borderRight: 0,
            }}
          >
            <Menu.Item icon={<HomeOutlined />} key="/home">
              <Link to="/home">数据概览</Link>
            </Menu.Item>
            <Menu.Item icon={<DiffOutlined />} key="/home/article">
              <Link to="/home/article">内容管理</Link>
            </Menu.Item>
            <Menu.Item icon={<EditOutlined />} key="/home/publish">
              <Link to="/home/publish">发布文章</Link>
            </Menu.Item>
          </Menu>
        </Sider>
        {/* 右侧：内容 */}
        <Layout
          className="scrollBox"
          style={{
            padding: '20px',
            overflowY: 'auto',
          }}
        >
          {/* 配置子路由 */}
          <Switch>
            <Route exact path="/home" component={Home} />
            <Route path="/home/article" component={Article} />
            <Route
              key={Date.now()}
              path="/home/publish/:id?"
              component={Publish}
            />
            <Route component={NotFound} />
          </Switch>
        </Layout>
      </Layout>
    </Layout>
  )
}

export default Layouts
