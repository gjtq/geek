import request from '@/utils/request'

export const getUserAction = (payload) => {
  return async (dispatch, getState) => {
    const { data } = await request.get('/user/profile')
    dispatch({ type: 'user/get', user: data })
  }
}
