import { delToken, setToken } from '@/utils/auth'
import request from '@/utils/request'

// 登录
export const loginAction = (values) => {
  return async (dispatch, getState) => {
    /**
     * 登录action
     * 1. 调用后台接口发送请求，获取token
     * 2. 使用dispatch存储token
     */
    // const {
    //   data: {
    //     data: { token },
    //   },
    // } = await axios.post('http://geek.itheima.net/v1_0/authorizations', values)
    const {
      data: { token },
    } = await request.post('/authorizations', values)
    dispatch({ type: 'login/token', token })
    // localStorage.setItem('geek-pc-token', token)
    setToken(token)
  }
}

// 退出登录
export const logoutAction = (payload) => {
  return async (dispatch, getState) => {
    dispatch({ type: 'login/delToken' })
    delToken()
    dispatch({ type: 'user/del' })
  }
}
