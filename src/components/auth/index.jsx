import { isAuth } from '@/utils/auth'
import { Route, Redirect } from 'react-router-dom'

function AuthRoute({ component: CurrComponent, ...rest }) {
  return (
    <Route
      //   path="/home"
      {...rest}
      render={(props) => {
        // 没有登录
        if (!isAuth()) {
          //   return <Redirect to="/login" />
          return (
            <Redirect
              to={{
                pathname: '/login',
                // 传递参数给登录页：当前访问页面的地址
                state: {
                  form: props.location.pathname,
                },
              }}
            />
          )
        }
        return <CurrComponent />
      }}
    />
  )
}

export default AuthRoute
