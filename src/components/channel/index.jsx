import { getChannelAction } from '@/store/actions/article'
import { Select } from 'antd'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
const { Option } = Select

function Channel({ value, onChange, width = 400 }) {
  const dispatch = useDispatch()
  const { channels } = useSelector((state) => state.article)
  useEffect(() => {
    dispatch(getChannelAction())
  }, [])
  return (
    <Select
      value={value}
      onChange={onChange}
      placeholder="请选择文章频道"
      // 默认选择lucy，暂时还没有受控
      // defaultValue="lucy"
      style={{ width }}
    >
      {/* <Option value="jack">Jack</Option>
              <Option value="lucy">Lucy</Option> */}
      {channels.map((item) => (
        <Option key={item.id} value={item.id}>
          {item.name}
        </Option>
      ))}
    </Select>
  )
}

export default Channel
