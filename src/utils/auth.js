const tokenKey = 'geek-pc-token'

// 存
export function setToken(token) {
  localStorage.setItem(tokenKey, token)
}

// 取
export function getToken() {
  return localStorage.getItem(tokenKey)
}

// 删
export function delToken() {
  localStorage.removeItem(tokenKey)
}

// 判断是否登录：true为登录
export function isAuth() {
  // !!使用场景：判断一个值是否是：'' null undefined
  return !!getToken()
}
