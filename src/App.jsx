import './App.scss'
// 导入页面
// import Login from '@/pages/login'
// import Layout from '@/pages/layout'
// import NotFound from '@/pages/404'
import { Router, Route, Switch, Redirect } from 'react-router-dom'
import AuthRoute from './components/auth'
import Test from './pages/test'
import customHistory from './utils/history'
import { lazy, Suspense } from 'react'
import { Spin } from 'antd'
// 导入配置路由需要的组件

// 懒加载
const Login = lazy(() => import('@/pages/login'))
const Layout = lazy(() => import('@/pages/layout'))
const NotFound = lazy(() => import('@/pages/404'))

function App() {
  return (
    <Router history={customHistory}>
      <Suspense
        fallback={
          <div className="loading">
            <Spin tip="页面努力加载中..." />
          </div>
        }
      >
        <div className="app">
          {/* 配置路由规则 */}
          <Switch>
            <Redirect exact from="/" to="/home" />
            {/* <Route path="/home" component={Layout} /> */}
            {/* <Route
            path="/home"
            render={() => {
              // 没有登录
              if (!isAuth()) {
                return <Redirect to="/login" />
              }
              return <Layout />
            }}
          /> */}
            <AuthRoute path="/home" component={Layout} />
            <Route path="/login" component={Login} />
            <Route path="/test" component={Test} />
            {/* 配置404页面必须放在最后，且不需要配置path属性 */}
            <Route component={NotFound} />
          </Switch>
        </div>
      </Suspense>
    </Router>
  )
}

export default App
