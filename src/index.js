/**
 * 入口
 */
import React from 'react'
import ReactDOM from 'react-dom/client'
// 全局样式
import '@/index.scss'
import App from '@/App'

// 集成store
import { Provider } from 'react-redux'
import store from './store'

// 国际化配置
// import 'moment/locale/zh-cn'
// import locale from 'antd/es/date-picker/locale/zh_CN'
// import zhCN from 'antd/locale/zh_CN'
import { ConfigProvider } from 'antd'
import 'dayjs/locale/zh-cn'
import locale from 'antd/locale/zh_CN'

const root = ReactDOM.createRoot(document.getElementById('root'))

if (process.env.NODE_ENV === 'production') {
  console.log = console.warn = () => {}
}

root.render(
  <Provider store={store}>
    <ConfigProvider locale={locale}>
      {/* <React.StrictMode> */}
      <App />
      {/* </React.StrictMode> */}
    </ConfigProvider>
  </Provider>
)
